<?php

namespace App\Controller;

use App\Repository\CustomerRepository;
use App\Repository\ProductRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @var string[]
     */
    private array $options;

    /**
     * @param array{template: string} $options
     */
    public function __construct(array $options)
    {
        $resolver = new OptionsResolver();
        $resolver->setRequired([
            'template',
        ]);

        $this->options = $resolver->resolve($options);
    }

    /**
     * @Route("/", name="home")
     */
    public function index(CustomerRepository $customerRepository, ProductRepository $productRepository): Response
    {
        return $this->render($this->options['template'], [
            'customer_count' => $customerRepository->count([]),
            'product_count' => $productRepository->count([]),
        ]);
    }
}
