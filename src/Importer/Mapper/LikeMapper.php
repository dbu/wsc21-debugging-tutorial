<?php

namespace App\Importer\Mapper;

use App\Entity\Customer;
use App\Repository\ProductRepository;
use RuntimeException;

class LikeMapper
{
    private ProductRepository $productRepository;

    public function __construct(ProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    /**
     * @param array<string, string> $data
     */
    public function map(Customer $customer, array $data): void
    {
        $product = $this->productRepository->find($data['ProductId']);
        if (!$product) {
            throw new RuntimeException(sprintf('Product %s not found', $data['ProductId']));
        }

        if (!$customer->likedProducts->contains($product)) {
            $customer->likedProducts->add($product);
        }
    }
}
