<?php

namespace App\Importer\Loader;

use Psr\Log\LoggerInterface;
use RuntimeException;
use function Safe\sprintf;
use function Safe\array_combine;

abstract class CsvLoader
{
    protected LoggerInterface $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param resource $handle
     * @param string[] $expectedHeader
     *
     * @return array<int, array<string, string>>
     */
    protected function readCsvFile($handle, array $expectedHeader): array
    {
        $header = fgetcsv($handle);
        if (!$header) {
            throw new RuntimeException(sprintf(
                'CSV file is completely empty, expected header [%s]',
                implode(',', $expectedHeader)
            ));
        }
        if (array_diff($expectedHeader, $header)) {
            throw new RuntimeException(sprintf(
                'Wrong header in CSV file. Expected [%s] but got [%s].',
                implode(',', $expectedHeader),
                implode(',', $header)
            ));
        }

        $data = [];
        $rowNumber = 1;
        while ($row = fgetcsv($handle)) {
            $rowNumber++;
            if (count($expectedHeader) !== count($row)) {
                $this->logger->warning('Invalid number of columns for {CSV} at CSV row {rowNumber}', [
                    'CSV' => get_class($this),
                    'rowNumber' => $rowNumber,
                ]);

                continue;
            }
            $data[] = array_combine($expectedHeader, $row);
        }

        return $data;
    }
}
