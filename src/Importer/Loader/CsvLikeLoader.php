<?php

namespace App\Importer\Loader;

use Psr\Log\LoggerInterface;
use RuntimeException;
use Safe\Exceptions\FilesystemException;
use function Safe\fopen;
use function Safe\fclose;

class CsvLikeLoader extends CsvLoader implements LikeLoader
{
    /**
     * @var string Absolute path to the csv like file
     */
    private string $file;

    /**
     * @param string          $csvFile Absolute path to the csv like file
     * @param LoggerInterface $logger
     */
    public function __construct(string $csvFile, LoggerInterface $logger)
    {
        parent::__construct($logger);
        $this->file = $csvFile;
    }

    public function loadLikes(): iterable
    {
        try {
            $handle = fopen($this->file, 'rb');
        } catch(FilesystemException $e) {
            throw new RuntimeException('Could not open CSV file '.$this->file, 0, $e);
        }

        $likes = $this->readCsvFile($handle, ['CustomerId', 'Name', 'ProductId']);
        fclose($handle);

        return $likes;
    }
}
