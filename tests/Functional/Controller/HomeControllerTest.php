<?php

namespace App\Tests\Functional\Controller;

use App\Tests\Functional\DatabasePurger;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{
    private KernelBrowser $client;

    public function setUp(): void
    {
        $this->client = static::createClient();

        DatabasePurger::purge(static::getContainer()->get('database_connection'));
    }

    public function testHomepage(): void
    {
        $crawler = $this->client->request('GET', '/');

        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        $this->assertStringContainsString(
            'Found 0 customers and 0 products in the database',
            $crawler->text()
        );
    }
}
