<?php

namespace App\Tests\Unit\Importer\Loader;

use App\Importer\Loader\CsvProductLoader;
use Monolog\Handler\TestHandler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use RuntimeException;

class CsvProductLoaderTest extends TestCase
{
    public function testLoadSuccess()
    {
        $expected = [
            [
                'Id' => 'CA06E2F2-AFB3-D0CB-2B96-63FD98E9C7AC',
                'Name' => 'dui',
                'Price' => '25',
            ],
            [
                'Id' => 'ABA23B3E-AC81-D1F7-E98C-B033987DC501',
                'Name' => 'orci lobortis augue',
                'Price' => '10',
            ],
        ];

        $loader = new CsvProductLoader(__DIR__.'/../../../fixtures/products_valid.csv', new NullLogger());
        $products = $loader->loadProducts();

        $this->assertEquals($expected, $products);
    }

    public function testIgnoreInvalidRow()
    {
        $expected = [
            [
                'Id' => 'CA06E2F2-AFB3-D0CB-2B96-63FD98E9C7AC',
                'Name' => 'dui',
                'Price' => '25',
            ],
            [
                'Id' => 'ABA23B3E-AC81-D1F7-E98C-B033987DC501',
                'Name' => 'orci lobortis augue',
                'Price' => '10',
            ],
        ];
        $handler = new TestHandler();
        $logger = new Logger('test', [$handler]);

        $loader = new CsvProductLoader(__DIR__.'/../../../fixtures/products_invalid_row.csv', $logger);
        $products = $loader->loadProducts();

        $this->assertEquals($expected, $products);
        $this->assertTrue($handler->hasWarningThatContains('Invalid number of columns'));
        $this->assertCount(1, $handler->getRecords());
    }

    public function testLoadFileNotFound()
    {
        $loader = new CsvProductLoader('/does/not/exist', new NullLogger());

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Could not open CSV file');

        // for this test only, we need to override the phpunit error handler that converts warnings to exceptions
        $previousErrorHandler = set_error_handler(static function() {});
        try {
            $loader->loadProducts();
        } finally {
            set_error_handler($previousErrorHandler);
        }
    }

    public function testLoadWrongHeaders()
    {
        $loader = new CsvProductLoader(__DIR__.'/../../../fixtures/likes_valid.csv', new NullLogger());

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Wrong header in CSV file');
        $loader->loadProducts();
    }
}
