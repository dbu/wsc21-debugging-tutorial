<?php

namespace App\Tests\Unit\Importer\Loader;

use App\Importer\Loader\CsvLikeLoader;
use Monolog\Handler\TestHandler;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;
use Psr\Log\NullLogger;
use RuntimeException;

class CsvLikeLoaderTest extends TestCase
{
    public function testLoadSuccess(): void
    {
        $expected = [
            [
                'CustomerId' => '1607080270099',
                'Name' => 'Quyn Goff',
                'ProductId' => 'CA06E2F2-AFB3-D0CB-2B96-63FD98E9C7AC',
            ],
            [
                'CustomerId' => '1605022133999',
                'Name' => 'Deborah Suarez',
                'ProductId' => 'CA06E2F2-AFB3-D0CB-2B96-63FD98E9C7AC',
            ],
            [
                'CustomerId' => '1605022133999',
                'Name' => 'Deborah Suarez',
                'ProductId' => 'ABA23B3E-AC81-D1F7-E98C-B033987DC501',
            ],
        ];

        $loader = new CsvLikeLoader(__DIR__.'/../../../fixtures/likes_valid.csv', new NullLogger());
        $likes = $loader->loadLikes();

        $this->assertSame($expected, $likes);
    }

    public function testIgnoreInvalidRow(): void
    {
        $expected = [
            [
                'CustomerId' => '1607080270099',
                'Name' => 'Quyn Goff',
                'ProductId' => 'CA06E2F2-AFB3-D0CB-2B96-63FD98E9C7AC',
            ],
            [
                'CustomerId' => '1605022133999',
                'Name' => 'Deborah Suarez',
                'ProductId' => 'CA06E2F2-AFB3-D0CB-2B96-63FD98E9C7AC',
            ],
            [
                'CustomerId' => '1605022133999',
                'Name' => 'Deborah Suarez',
                'ProductId' => 'ABA23B3E-AC81-D1F7-E98C-B033987DC501',
            ],
        ];
        $handler = new TestHandler();
        $logger = new Logger('test', [$handler]);

        $loader = new CsvLikeLoader(__DIR__.'/../../../fixtures/likes_invalid_row.csv', $logger);
        $likes = $loader->loadLikes();

        $this->assertSame($expected, $likes);
        $this->assertTrue($handler->hasWarningThatContains('Invalid number of columns'));
        $this->assertCount(1, $handler->getRecords());
    }

    public function testLoadFileNotFound(): void
    {
        $loader = new CsvLikeLoader('/does/not/exist', new NullLogger());
        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Could not open CSV file');

        // for this test only, we need to override the phpunit error handler that converts warnings to exceptions
        $previousErrorHandler = set_error_handler(static function() {});
        try {
            $loader->loadLikes();
        } finally {
            set_error_handler($previousErrorHandler);
        }
    }

    public function testLoadWrongHeaders(): void
    {
        $loader = new CsvLikeLoader(__DIR__.'/../../../fixtures/products_valid.csv', new NullLogger());

        $this->expectException(RuntimeException::class);
        $this->expectExceptionMessage('Wrong header in CSV file');
        $loader->loadLikes();
    }
}
