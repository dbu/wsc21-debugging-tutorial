<?php

namespace App\Tests\Unit\Importer\Mapper;

use App\Entity\Product;
use App\Importer\Mapper\ProductMapper;
use App\Repository\ProductRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class ProductMapperTest extends TestCase
{
    /**
     * @var ProductRepository&MockObject
     */
    private ProductRepository $repository;

    protected function setUp(): void
    {
        $this->repository = $this->createMock(ProductRepository::class);
    }

    public function testCreateProduct(): void
    {
        $this->repository
            ->expects($this->once())
            ->method('find')
            ->with('ABA23B3E-AC81-D1F7-E98C-B033987DC501')
            ->willReturn(null)
        ;

        $mapper = new ProductMapper($this->repository);
        $product = $mapper->map([
            'Id' => 'ABA23B3E-AC81-D1F7-E98C-B033987DC501',
            'Name' => 'orci lobortis augue',
            'Price' => '10',
        ]);

        $this->assertSame('ABA23B3E-AC81-D1F7-E98C-B033987DC501', $product->id);
        $this->assertSame('orci lobortis augue', $product->name);
        $this->assertSame(10, $product->price);
    }

    public function testUpdateProduct(): void
    {
        $existingProduct = new Product();
        $existingProduct->id = 'ABA23B3E-AC81-D1F7-E98C-B033987DC501';
        $existingProduct->name = 'Old Name';
        $existingProduct->price = 12;

        $this->repository
            ->expects($this->once())
            ->method('find')
            ->with('ABA23B3E-AC81-D1F7-E98C-B033987DC501')
            ->willReturn($existingProduct)
        ;

        $mapper = new ProductMapper($this->repository);
        $product = $mapper->map([
            'Id' => 'ABA23B3E-AC81-D1F7-E98C-B033987DC501',
            'Name' => 'orci lobortis augue',
            'Price' => '10',
        ]);

        $this->assertSame($existingProduct, $product);
        $this->assertSame('orci lobortis augue', $product->name);
        $this->assertSame(10, $product->price);
    }
}
